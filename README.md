# 1896 Gallery

[![GitHub](https://img.shields.io/badge/GitHub-Accudio-0366d6.svg)](https://github.com/Accudio) [![Twitter](https://img.shields.io/badge/Twitter-@accudio-1DA1F2.svg)](https://twitter.com/accudio) [![Website](https://img.shields.io/badge/Website-alistairshepherd.uk-4B86AF.svg)](https://alistairshepherd.uk)

Website for the 1896 Gallery using 11ty and WordPress as headless CMS. Built by [Accudio][accudiourl].

## Version History

- v2.0.0 - Migrated from Gridsome to Eleventy
- v1.0.2 - Added ability to open consent manager using link with href="#consent"
- v1.0.1 - Added analytics using Countly
- v1.0.0 - Initial public release

## License

Copyright &copy; 2020 [Alistair Shepherd][alistairurl].

[accudiourl]:https://accudio.com
[alistairurl]:https://alistairshepherd.uk
