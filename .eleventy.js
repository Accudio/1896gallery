const path = require('path')
const alias = require('module-alias')

const sitemap = require('eleventy-plugin-sitemap')
const sitemapItems = require('./src/_includes/utils/collections/sitemap-items')

// twelvety options can be found in .twelvety.js
// set up alias for Twelvety options
alias.addAlias('@12ty', path.join(__dirname, '.twelvety'))

// you can now require Twelvety options using @12ty
const twelvety = require('@12ty')

// filters, transforms and shortcodes can be found in utils
const addFilters = require('./src/_includes/utils/filters')
const addTransforms = require('./src/_includes/utils/transforms')
const addShortcodes = require('./src/_includes/utils/shortcodes')

// site config
const siteConfig = require('./src/_data/config.json')

module.exports = function (config) {
  addFilters(config)
  addTransforms(config)
  addShortcodes(config)

  /**
   * Passthrough file copy
   */
  config.addPassthroughCopy({ 'static': '.' })
  config.addPassthroughCopy({ 'node_modules/photoswipe/dist/default-skin/*.{png,svg,gif}': '_assets/' })

  /**
   * Collections
   */
  config.addCollection('sitemapItems', sitemapItems)

  /**
   * Add Plugins
   */
  // config.addPlugin(errorOverlay)
  config.addPlugin(sitemap, {
    lastModifiedProperty: 'modified',
    sitemap: {
      hostname: siteConfig.url
    }
  })

  // Deep merge when combining the Data Cascade
  // Documentation: https://www.11ty.dev/docs/data-deep-merge/
  config.setDataDeepMerge(true)

  // Options for LiquidJS
  // Documentation: https://liquidjs.com/tutorials/options.html
  config.setLiquidOptions({
    dynamicPartials: true,
    strict_filters: false,
    strict_variables: false
  })

  return {
    dir: twelvety.dir
  }
}
