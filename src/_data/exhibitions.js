const dayjs = require('dayjs')
const AdvancedFormat = require('dayjs/plugin/advancedFormat')

dayjs.extend(AdvancedFormat)

const fetchData = require('../_includes/utils/helpers/fetchData.js')

const path = 'wp/v2/exhibitions?per_page=10'

module.exports = async function fetchExhibitions() {
  const exhibitions = await fetchData('exhibitions', path)
  const future = sortDate(exhibitions.filter(function(exhibition) {
    return dayjs().isBefore(dayjs(exhibition.acf.dates.start_date))
  }))
  const current = sortDate(exhibitions.filter(function(exhibition) {
    let startDate = dayjs(exhibition.acf.dates.start_date)
    let endDate = dayjs(exhibition.acf.dates.end_date)
    return dayjs() >= startDate && dayjs() <= endDate
  }))
  const past = sortDate(exhibitions.filter(function(exhibition) {
    return dayjs().isAfter(dayjs(exhibition.acf.dates.end_date))
  }))

  return {
    all: exhibitions,
    future,
    current,
    past,
    notPast: [...future, ...current]
  }
}

function sortDate(exhibitions) {
  return exhibitions.sort(function(a, b) {
    const aStart = dayjs(a.acf.dates.start_date)
    const bStart = dayjs(b.acf.dates.start_date)
    return aStart.isBefore(bStart) ? -1 : bStart.isBefore(aStart) ? 1 : 0
  })
}
