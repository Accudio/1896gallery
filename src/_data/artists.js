const fetchData = require('../_includes/utils/helpers/fetchData.js')

const path = 'wp/v2/artists?per_page=10'

module.exports = async function fetchArtists() {
  return fetchData('artists', path)
}
