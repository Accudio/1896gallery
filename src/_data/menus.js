const fetchData = require('../_includes/utils/helpers/fetchData.js')
const indexed = require('../_includes/utils/helpers/indexed.js')

const path = 'menus/v1/menus'

module.exports = async function fetchMenus() {
  const menus = await fetchData('menus', path)
  return indexed(menus, 'location')
}
