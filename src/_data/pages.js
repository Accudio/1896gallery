const fetchData = require('../_includes/utils/helpers/fetchData.js')

const path = 'wp/v2/pages?per_page=10'

module.exports = async function fetchPages() {
  return fetchData('pages', path)
}
