const fetchData = require('../_includes/utils/helpers/fetchData.js')

const path = 'accudio/v1/options'

module.exports = async function fetchOptions() {
  return fetchData('options', path)
}
