const pages = require('./pages')

const config = require('./config.json')

module.exports = async function fetchGeneratedPages() {
  const allPages = await pages()
  const generatedPages = []
  allPages.forEach(item => {
    if (!config.genPagesExclude.includes(item.slug)) {
      generatedPages.push(item)
    }
  })
  return generatedPages
}
