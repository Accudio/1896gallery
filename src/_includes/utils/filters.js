const dayjs = require('dayjs')

module.exports = function(config) {
  // JSON.stringify filter
  config.addFilter('stringify', function(value) {
    return JSON.stringify(value)
  })

  // index collection based on slug property
  config.addFilter('index', function(collection) {
    const items = {}
    collection.forEach(val => {
      items[val['slug']] = val
    })
    return items
  })

  // leadText for Home Carousel
  config.addFilter('homeCarouselLead', function(startDate) {
    return dayjs().isBefore(dayjs(startDate)) ? 'Upcoming' : 'Current'
  })

  // short desciption
  config.addFilter('shortDesc', function(description) {
    return description ? description.split('<p><!--more--></p>')[0] : ''
  })

  // long description
  config.addFilter('longDesc', function(description) {
    return description ? description.replace('<p><!--more--></p>', '') : ''
  })

  // footer copyright
  config.addFilter('copyright', function(copyright) {
    return copyright.replace('[Y]', new Date().getFullYear())
  })

  // remove whitespace from phone number
  config.addFilter('phone', function(phone) {
    if (!phone) return
    return phone.replace(/\s/g, '')
  })
}
