const images = require('../helpers/images')

module.exports = function addImageShortcodes(config) {
  config.addShortcode('wpImage', images.wpImage)
  config.addShortcode('localImage', images.localImage)

  config.addShortcode('urlImage', images.urlImage)
}
