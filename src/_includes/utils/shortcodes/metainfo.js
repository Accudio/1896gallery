const config = require('../../../_data/config.json')

const images = require('../helpers/images')

module.exports = function(wp, socialImage, page) {
  let output = ''

  const url = `${config.url}${page.url}`

  /**
   * general
   */
  // title tag
  const title = wp.yoast_title || `${wp.title.rendered} – ${config.site}`
  output += `<title>${title}</title>`

  // description tag
  output += getYoast(wp.yoast_meta, 'description')

  // canonical
  output += `<link rel="canonical" href="${url}"/>`

  /**
   * open graph
   */
  // og locale
  output += '<meta property="og:locale" content="en_GB"/>'

  // og type
  output += '<meta property="og:type" content="article"/>'

  // og title
  output += getYoast(wp.yoast_meta, 'og:title', 'property')

  // og description
  output += getYoast(wp.yoast_meta, 'og:description', 'property')

  // og url
  output += `<meta property="og:url" content="${url}"/>`

  // og site_name
  output += `<meta property="og:site_name" content="${config.site}"/>`

  // modified time
  output += getYoast(wp.yoast_meta, 'twitter:card', 'property')

  // image
  const yoastImage = wp.yoast_meta.find(item => item.property === 'og:image')
  if (yoastImage) {
    // open graph social image from yoast
    const yoastImageUrl = images.urlImage(yoastImage.content)
    output += `<meta property="og:image" content="${yoastImageUrl}"/>`
    output += getYoast(wp.yoast_meta, 'og:image:width', 'property')
    output += getYoast(wp.yoast_meta, 'og:image:height', 'property')
  } else {
    if (socialImage) {
      // open graph social image from 11ty
      const socialImageUrl = images.urlImage(socialImage.url)
      output += `<meta property="og:image" content="${socialImageUrl}"/>`
      output += `<meta property="og:image:width" content="${socialImage.width}"/>`
      output += `<meta property="og:image:height" content="${socialImage.height}"/>`
    } else {
      // fallback open graph image
      output += `
        <meta property="og:image" content="${config.url}/open-graph.png"/>
        <meta property="og:image:alt" content="Painting of 1896 Gallery from externally. It is a large stone hall with purple trim along the roof, several people are grouped in front of the door."/>
        <meta property="og:image:width" content="1200"/>
        <meta property="og:image:height" content="630"/>
      `
    }
  }

  /**
   * twitter cards
   */
  // card type
  output += getYoast(wp.yoast_meta, 'article:modified_time')

  // twitter site
  output += `<meta property="twitter:site" content="${config.site}"/>`

  // title
  output += getYoast(wp.yoast_meta, 'twitter:title')

  // description
  output += getYoast(wp.yoast_meta, 'twitter:description')

  // image
  const twitterImage = wp.yoast_meta.find(item => item.property === 'twitter:image')
  if (twitterImage) {
    output += `<meta property="twitter:image" content="${twitterImage.content}"/>`
  }

  /**
   * misc
   */
  // robots
  output += getYoast(wp.yoast_meta, 'robots')

  return output
}

function getYoast(yoast, key, field = 'name') {
  const item = yoast.find(item => item[field] === key)
  if (!item) return ''

  return `<meta ${field}="${key}" content="${item.content}"/>`
}
