const path = require('path')
const sass = require('node-sass')
const postcss = require('postcss')
const postcssrc = require('postcss-load-config')

// twelvety options from .twelvety.js
const twelvety = require('@12ty')

// render styles using node-sass
// documentation: https://github.com/sass/node-sass
function renderStyles(data) {
  return new Promise((resolve, reject) => {
    sass.render({
      data,
      // allow `@import` from files within styles directory and node modules
      includePaths: [
        path.join(process.cwd(), twelvety.dir.input, twelvety.dir.styles),
        path.join(process.cwd(), 'node_modules')
      ],
      // set `indentedSyntax` to true if you want to use indented sass
      indentedSyntax: false
    }, (error, result) => {
      if (error) {
        reject(error)
      }
      resolve(result.css.toString())
    })
  })
}

module.exports = function(config) {
  // each stylesheet is stored within an array for its given 'chunk'
  const STYLES = {}

  // store each stylesheet within its chunk
  // the chunk defaults to the URL of the current page
  // use language 'scss' for Liquid highlighting
  config.addPairedShortcode('stylesheet', function(content, _language, chunk = this.page.url) {
    // make sure that the chunk exists
    if (!STYLES.hasOwnProperty(chunk)) {
      STYLES[chunk] = []
    }

    // add the stylesheet to the chunk, if it's not already in it
    if (!STYLES[chunk].includes(content)) {
      STYLES[chunk].push(content)
    }

    return ''
  })

  // render the styles for the given chunk
  config.addShortcode('styles', async function(chunk = this.page.url) {
    // if there aren't any styles, just return nothing
    if (!STYLES.hasOwnProperty(chunk)) {
      return ''
    }

    // join all the styles in the chunk
    const joined = STYLES[chunk].join('\n')

    // render sass using node-sass
    let rendered = await renderStyles(joined)

    // if production, run through postcss
    if (twelvety.env === 'production') {
      // input path used by PostCSS
      const from = path.resolve(process.cwd(), this.page.inputPath)

      // use postcss-load-config to get plugins and options
      const { plugins, options } = postcssrc.sync({ from })
      rendered = postcss(plugins).process(rendered, options)
    }

    return rendered
  })

  // reset all styles on re-runs
  config.on('beforeWatch', function() {
    for (const chunk in STYLES) {
      delete STYLES[chunk]
    }
  })

  // watch the styles directory
  config.addWatchTarget(path.join(process.cwd(), twelvety.dir.input, twelvety.dir.styles))
}
