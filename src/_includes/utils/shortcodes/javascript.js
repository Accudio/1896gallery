const browserify = require('browserify')
const babel = require('@babel/core')
const path = require('path')

// readable stream for browserify in-memory
const { Readable } = require('stream')

// twelvety options from .twelvety.js
const twelvety = require('@12ty')

// bundle scripts with browserify
// documentation: https://github.com/browserify/browserify
function bundleScripts(data) {
  return new Promise((resolve, reject) => {
    // pass browserify a ReadableStream of the script
    let temp = browserify([Readable.from(data)], {
      paths: [
        path.join(process.cwd(), twelvety.dir.input, twelvety.dir.js),
        path.join(process.cwd(), 'node_modules')
      ]
    })

    // tinyify plugin provides various optimisations
    if (twelvety.env === 'production') {
      temp = temp.plugin('tinyify')
    }

    // bundle code for browser
    temp.bundle((error, buffer) => {
      if (error) {
        reject(error)
      }
      resolve(buffer)
    })
  })
}

module.exports = function(config) {
  // each script is stored within an array for its given 'chunk'
  const SCRIPTS = {}

  // store each script within its chunk
  // the chunk defaults to the URL of the current page
  config.addPairedShortcode('javascript', function(content, chunk = this.page.url) {
    // make sure that the chunk exists
    if (!SCRIPTS.hasOwnProperty(chunk)) {
      SCRIPTS[chunk] = []
    }

    // add the script to the chunk, if it's not already in it
    if (!SCRIPTS[chunk].includes(content)) {
      SCRIPTS[chunk].push(content)
    }

    return ''
  })

  // render the scripts for the given chunk
  config.addShortcode('script', async function(chunk = this.page.url) {
    // if there aren't any scripts, just return nothing
    if (!SCRIPTS.hasOwnProperty(chunk)) {
      return ''
    }

    // wrap scripts in iife and join all the scripts in chunk
    const joined = SCRIPTS[chunk].map((data) => `;(() => {\n${data}\n})()`).join('\n')

    // bundle the scripts using browserify
    const bundled = await bundleScripts(joined)

    // use babel with babel-preset-env for compatibility
    return babel.transformSync(bundled).code
  })

  // reset all scripts on re-runs
  config.on('beforeWatch', function() {
    for (const chunk in SCRIPTS) {
      delete SCRIPTS[chunk]
    }
  })
}
