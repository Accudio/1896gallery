const fs = require('fs')
const path = require('path')
const crypto = require('crypto')

// twelvety options from .twelvety.js
const twelvety = require('@12ty')

// minify functions
const minify = require('../minify')

// size of asset hash
const SIZE = 8

// hash function
function hashContent(content) {
  return crypto
    .createHash('md5')
    .update(content)
    .digest('hex')
    .slice(0, SIZE)
}

module.exports = function(content, extension) {
  // minify content if applicable
  if (['css', 'js', 'html'].includes(extension)) { content = minify[extension](content) }

  // hash content
  const hash = hashContent(content)

  // output assets directory
  const assetsDir = path.join(process.cwd(), twelvety.dir.output, twelvety.dir.assets)

  // ensure the assets folder exists
  if (!fs.existsSync(assetsDir)) {
    fs.mkdirSync(assetsDir, {
      recursive: true
    })
  }

  // save hashed asset file
  const filename = `${hash}.${extension}`
  fs.writeFileSync(path.join(assetsDir, filename), content)

  // output root path from output directory
  return path.posix.join('/', twelvety.dir.assets, filename)
}

module.exports.hashContent = hashContent
