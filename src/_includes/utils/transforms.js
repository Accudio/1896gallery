const append = require('./transforms/append')
const critical = require('./transforms/critical')
const noscript = require('./transforms/noscript')

// require local minify functions
const minify = require('./minify')

module.exports = function(config) {
  config.addTransform('append', append)
  config.addTransform('critical', critical)
  config.addTransform('noscript', noscript)

  config.addTransform('format', function(content, outputPath) {
    if (outputPath && outputPath.endsWith('.html')) {
      return minify.html(content)
    }

    return content
  })
}
