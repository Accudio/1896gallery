const Cleancss = require('clean-css')
const uglify = require('uglify-js')
const htmlmin = require('html-minifier')
const beautify = require('js-beautify')

// twelvety options from .twelvety.js
const twelvety = require('@12ty')

// use beautify in development
// options: https://github.com/beautify-web/js-beautify
const BEAUTIFY_OPTIONS = {
  extra_liners: [],
  indent_inner_html: true,
  indent_size: 2,
  max_preserve_newlines: 1
}

function minifyCSS(content, type) {
  // ignore inline and media types
  if (['media', 'inline'].includes(type)) { return content }

  if (twelvety.env === 'production') {
    // clean-css
    // Options: https://github.com/jakubpawlowicz/clean-css
    return new Cleancss({
      level: {
        1: {
          specialComments: 0
        },
        2: true
      }
    }).minify(content).styles
  } else {
    return beautify.css(content, BEAUTIFY_OPTIONS)
  }
}

function minifyJS(content) {
  if (twelvety.env === 'production') {
    // uglify
    // Options: https://github.com/mishoo/UglifyJS
    return uglify.minify(content).code
  } else {
    return beautify.js(content, BEAUTIFY_OPTIONS)
  }
}

function minifyHTML(content) {
  if (twelvety.env === 'production') {
    // html-minifier
    // Options: https://github.com/kangax/html-minifier
    return htmlmin.minify(content, {
      collapseBooleanAttributes: true,
      collapseInlineTagWhitespace: true,
      collapseWhitespace: true,
      conservativeCollapse: true,
      minifyCSS,
      minifyJS,
      removeComments: true,
      useShortDoctype: true
    })
  } else {
    return beautify.html(content, BEAUTIFY_OPTIONS)
  }
}

module.exports = {
  css: minifyCSS,
  js: minifyJS,
  html: minifyHTML
}
