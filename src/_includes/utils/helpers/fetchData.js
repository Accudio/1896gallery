const flatcache = require('flat-cache')
const path = require('path')

const settings = require('../../../_data/config.json')

const getNumPages = require('./getNumPages.js')
const getCacheKey = require('./getCacheKey.js')
const fetchAll = require('./fetchAll.js')
const fetch = require('./fetch.js')

module.exports = async function fetchData(type, URLpath) {
  const cache = flatcache.load(type, path.resolve('./src/_datacache'))
  const key = getCacheKey()
  const cachedData = cache.getKey(key)

  const endPoint = `${settings.wp}/wp-json/${URLpath}`

  // fetch again if we don't have cache
  if (!cachedData) {
    // if type is options/menus
    if (type === 'options' || type === 'menus') {
      // get result
      const result = await fetch(endPoint)

      // set and save cache
      cache.setKey(key, result)
      cache.save()

      return result
    }

    // get number of pages
    const numPages = await getNumPages(endPoint)

    // fetch all
    const allPosts = await fetchAll(numPages, endPoint)

    // set and save cache
    cache.setKey(key, allPosts)
    cache.save()
    return allPosts
  }

  // and return them
  return cachedData
}
