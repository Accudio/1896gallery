const axios = require('axios')

module.exports = async function fetch(endPoint) {
  return axios
    .get(endPoint)
    .then(function(response) {
      return response.data
    })
    .catch(function(error) {
      console.log( error ); /* eslint-disable-line */
    })
}
