module.exports = function(array, field) {
  const items = {}
  array.forEach(val => {
    items[val[field]] = val
  })
  return items
}
