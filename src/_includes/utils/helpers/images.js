const path = require('path')
const sizeOf = require('image-size')

const config = require('../../../_data/config.json')

const isNetlify = process.env.NETLIFY === 'true'

// default sizes attribute
const SIZE_PROFILES = {
  default: '(min-width:1200px) 253px, (min-width:992px) 208px, (min-width:768px) 148px, (min-width:576px) 226px, calc(50vw - 63px)',
  gallery: '(min-width:1200px) 270px, (min-width:992px) 225px, (min-width:768px) 165px, (min-width:576px) 238px, calc(50vw - 41px)'
}

// srcset generation
const SRCSET_MIN = 200
const SRCSET_STEP = 100
const SRCSET_MAX = 2000

/**
 * cdnSrc
 *
 * return url for src
 *
 * @param {array}         image
 * @param {number, bool}  width=false
 * @param {number, bool}  height=false
 * @returns {string}
 */
function cdnSrc(url, width = false, height = false) {
  return cdnUrl(url, width, height)
}

/**
 * srcset
 *
 * generate srcset from srcset widths
 *
 * @param {array}         image
 * @param {number, bool}  ratio=false
 * @returns {string}
 */
function cdnSrcset(url, width, ratio = false) {
  const widths = srcsetWidths(width)
  if (widths.length < 2) return

  const srcset = widths.map(width => {
    const height = ratio ? Math.ceil(width * ratio) : false
    const src = cdnUrl(url, width, height)
    return `${src} ${width}w`
  })

  return srcset.join(',')
}

/**
 * srcsetWidths
 *
 * generate widths for srcset
 *
 * @param {number} nativeWidth
 * @returns {string}
 */
function srcsetWidths(nativeWidth) {
  const maxWidth = Math.min(nativeWidth, SRCSET_MAX)

  const widths = []
  let width = SRCSET_MIN
  while (width <= maxWidth) {
    widths.push(width)
    width += SRCSET_STEP
  }

  if (nativeWidth < SRCSET_MAX) {
    widths.push(nativeWidth)
  }

  // if there is no sizes generated
  if (!widths) return false

  return widths
}

/**
 * cdnUrl
 *
 * generate url for use with cdn
 *
 * @param {string}        origUrl
 * @param {number, bool}  width
 * @param {number, bool}  height
 * @returns {any}
 */
function cdnUrl(origUrl, width, height) {
  const url = prepUrl(origUrl)

  switch (config.cdn.provider) {
  case 'cloudimage':
    return cloudimageUrl(url, width, height)
  }
}

/**
 * prepUrl
 *
 * make preparations to the URL to be passed onto cdn prep
 *
 * @param {string} url
 * @returns {string}
 */
function prepUrl(url) {
  switch (config.cdn.provider) {
  case 'cloudimage':
    return cloudimagePrepUrl(url)
  }
}

/**
 * cloudimageUrl
 *
 * returns url for use with cloudimage
 *
 * @param {string}        image
 * @param {number, bool}  width
 * @param {number, bool}  height
 * @returns {string}
 */
function cloudimageUrl(image, width, height) {
  const args = []

  // width and height
  if (width) args.push(`w=${width}`)
  if (height) args.push(`h=${height}`)

  const query = args.length ? '?' + args.join('&') : ''

  // if this is production, use Netlify proxying for image cdn
  if (true) {
    return `/${image}${query}`
  }

  // in dev, use cloudimage with full url
  return `${config.cdn.url}${image}${query}`
}

/**
 * cloudimagePrepUrl
 *
 * prepare URL for use with cloudimage
 *
 * @param {string} url
 * @returns {string}
 */
function cloudimagePrepUrl(url) {
  // wordpress uploads
  const uploads = `${config.wp}/wp-content/uploads`
  url = url.replace(uploads, '_up_')

  // local images
  const local = `${config.url}/images`
  url = url.replace(local, '_lc_')

  return url
}

/**
 * picture
 *
 * generates picture element
 *
 * @date 2020-09-24
 * @param {string}        url
 * @param {string}        alt
 * @param {string}        className
 * @param {number, bool}  fallbackWidth
 * @param {number, bool}  fallbackHeight
 * @param {number}        nativeWidth
 * @param {number}        nativeHeight
 * @param {number, bool}  aspectRatio
 * @param {string}        sizes
 * @param {string}        loading
 * @returns {string}
 */
function picture(
  url,
  alt,
  className,
  fallbackWidth,
  fallbackHeight,
  nativeWidth,
  nativeHeight,
  aspectRatio,
  sizes,
  loading
) {
  const fallbackSrc = cdnSrc(url, fallbackWidth, fallbackHeight)
  const srcset = cdnSrcset(url, nativeWidth, aspectRatio)

  // calculate aspect ratio
  // const ratio = aspectRatio || (nativeHeight / nativeWidth)

  // generate source if srcset exists
  const sourcesSizes = SIZE_PROFILES[sizes] || sizes
  const source = srcset ? `<source srcset="${srcset}" sizes="${sourcesSizes}">` : ''

  // picture element
  const picture = `
    <picture class="${className}--pic">
      ${source}
      <img class="${className}" src="${fallbackSrc}" alt="${alt}" loading="${loading}" width="${nativeWidth}" height="${nativeHeight}">
    </picture>
  `

  return picture
}

/**
 * wpImage
 *
 * parses acf image data and passes to image function
 *
 * @param {array}         image
 * @param {string}        className=''
 * @param {number, bool}  fallbackWidth=false
 * @param {number, bool}  aspectRatio=false
 * @param {string, bool}  alt=false
 * @param {string}        sizes=SIZE_PROFILES.default
 * @param {string}        loading='lazy'
 * @returns {string}
 */
function wpImage(
  image,
  className = '',
  fallbackWidth = false,
  aspectRatio = false,
  alt = false,
  sizes = SIZE_PROFILES.default,
  loading = 'lazy'
) {
  // calculate fallbackHeight from fallbackWidth
  const fallbackHeight = aspectRatio && fallbackWidth ? Math.ceil(fallbackWidth * aspectRatio) : false

  return picture(
    image.url,
    alt || image.alt || '',
    className,
    fallbackWidth,
    fallbackHeight,
    image.width,
    image.height,
    aspectRatio,
    sizes,
    loading
  )
}

/**
 * localImage
 *
 * parses local image data and passes to image function
 *
 * @param {array}         image
 * @param {string}        className=''
 * @param {number, bool}  fallbackWidth=false
 * @param {number, bool}  aspectRatio=false
 * @param {string, bool}  alt=false
 * @param {string}        sizes=SIZE_PROFILES.default
 * @param {string}        loading='lazy'
 * @returns {string}
 */
function localImage(
  address,
  alt = '',
  className = '',
  fallbackWidth = false,
  aspectRatio = false,
  sizes = SIZE_PROFILES.default,
  loading = 'lazy'
) {
  // get url
  const url = `${config.url}/images/${address}`

  // get path
  const imagePath = path.join(process.cwd(), 'static/images/', address)
  const dimensions = sizeOf(imagePath)

  // calculate fallbackHeight from fallbackWidth
  const fallbackHeight = aspectRatio && fallbackWidth ? Math.ceil(fallbackWidth * aspectRatio) : false

  return picture(
    url,
    alt,
    className,
    fallbackWidth,
    fallbackHeight,
    dimensions.width,
    dimensions.height,
    aspectRatio,
    sizes,
    loading
  )
}

/**
 * urlImage
 *
 * takes image name and generates cdn image url
 *
 * @param {string}        image
 * @param {number, bool}  width=false
 * @param {number, bool}  height=false
 * @returns {string}
 */
function urlImage(url, width = false, height = false) {
  // if url does not begin with http if is local, set local url
  if (!url.startsWith('http')) {
    url = `${config.url}/images/${url}`
  }

  // prep url for cdn
  url = cdnUrl(url, width, height)

  return url
}

module.exports = {
  SIZE_PROFILES,
  cdnSrc,
  cdnSrcset,
  srcsetWidths,
  cdnUrl,
  prepUrl,
  cloudimageUrl,
  cloudimagePrepUrl,
  picture,
  wpImage,
  localImage,
  urlImage
}
