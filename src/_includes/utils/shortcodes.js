const addStylesheetShortcodes = require('./shortcodes/stylesheet')
const addJavascriptShortcodes = require('./shortcodes/javascript')
const addImageShortcodes = require('./shortcodes/images')
const assetShortcode = require('./shortcodes/asset')
const metainfoShortcode = require('./shortcodes/metainfo')

const dayjs = require('dayjs')
const AdvancedFormat = require('dayjs/plugin/advancedFormat')
dayjs.extend(AdvancedFormat)

// instance of markdown-it
const markdown = require('./markdown')

module.exports = function(config) {
  addStylesheetShortcodes(config)
  addJavascriptShortcodes(config)
  addImageShortcodes(config)

  config.addShortcode('asset', assetShortcode)
  config.addShortcode('metainfo', metainfoShortcode)

  // helper for the append transform
  config.addPairedShortcode('append', function(content, selector) {
    return `<template data-append="${selector}">\n${content}\n</template>`
  })

  // markdown paired shortcode, useful for including markdown files
  config.addPairedShortcode('markdown', function(content) {
    return markdown.render(content)
  })

  // defer css
  config.addShortcode('deferCSS', function(url, priority = 'low', noscript = true) {
    let preload = ''
    let fallback = ''

    // if priority is high, add preload to raise browser priority
    if (priority === 'high') {
      preload = `<link rel="preload" href="${url}" as="style">`
    }

    // if noscript should be included
    if (noscript) {
      fallback = `<noscript><link rel="stylesheet" href="${url}"></noscript>`
    }

    return `
      ${preload}
      <link rel="stylesheet" href="${url}" media="print" onload="this.media='all'">
      ${fallback}
    `
  })

  // timespan for exhibitions
  config.addShortcode('exhibitionTimespan', function(startDate, endDate) {
    let start = dayjs(startDate)
    let end = dayjs(endDate)
    if (start.get('month') === end.get('month')) {
      return 'Running from ' + start.format('Do') + ' to ' + end.format('Do MMMM')
    } else {
      return 'Running from ' + start.format('Do MMMM') + ' to ' + end.format('Do MMMM')
    }
  })

  // exhibition plural text
  config.addShortcode('exhibitionPlural', function(length, text) {
    text = text || 'Exhibition'
    return length === 1 ? text : text + 's'
  })

  // work price text
  config.addShortcode('workPrice', function(availability, price) {
    if (price) {
      if (availability === 'sold') {
        return 'SOLD'
      }
      return '£' + parseFloat(price).toFixed(2)
    }
    return ''
  })

  // only output class if current menu item is active
  config.addPairedShortcode('navActive', function(content, url, item) {
    if (item === '/' && url !== '/') return ''

    return url.startsWith(item) ? content : ''
  })
}
