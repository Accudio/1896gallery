const dayjs = require('dayjs')

const pages = require('../../../_data/pages')
const artists = require('../../../_data/artists')

module.exports = async function sitemapItems(collections) {
  // get items from filesystem
  const fileItems = {}
  collections.getAll().forEach(item => {
    const entry = {
      url: item.url,
      data: {
        modified: dayjs(item.date).format(),
        sitemap: item.data.sitemap
      }
    }
    fileItems[item.url] = entry
  })

  // get items from WordPress
  const wpItems = {}
  const allPages = await pages()
  const allArtists = await artists()

  let allWp = [
    ...allPages,
    ...allArtists
  ]

  allWp = allWp.filter(item => {
    item.yoast_meta.forEach(meta => {
      if (meta.name && meta.name === 'robots' && meta.content) {
        const robots = meta.content.split(', ')
        if (robots[0] === 'noindex') {
          return false
        }
      }
    })
    return true
  })
  allWp.forEach(item => {
    let url = item.slug === 'home' ? '/' : `/${item.slug}/`
    if (item.type === 'artists') {
      url = `/artists/${item.slug}/`
    }
    wpItems[url] = {
      url: url,
      data: {
        modified: dayjs(item.modified).format()
      }
    }
  })

  // merge filesystem and WordPress items
  let items = {
    ...fileItems
  }

  Object.values(wpItems).forEach(item => {
    if (items[item.url]) {
      items[item.url].data.modified = item.data.modified
    } else {
      items[item.url] = item
    }
  })

  // return array of items
  return Object.values(items)
}
