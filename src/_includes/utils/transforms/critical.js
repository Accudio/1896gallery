const { JSDOM } = require('jsdom')
const critical = require('critical')

// post-processing
const postcss = require('postcss')
const autoprefixer = require('autoprefixer')
const cssnano = require('cssnano')

// Twelvety options from .twelvety.js
const twelvety = require('@12ty')

module.exports = async function(content, outputPath) {
  if (outputPath && outputPath.endsWith('.html') && twelvety.env === 'production') {
    let { css } = await critical.generate({
      base: twelvety.dir.output,
      html: content,
      minify: false,
      width: 2560,
      height: 1600,
      dimensions: [
        {
          // average smartphone
          width: 375,
          height: 667
        },
        {
          // big smartphone
          width: 640,
          height: 1136
        },
        {
          // small laptop
          width: 1024,
          height: 768
        },
        {
          // average desktop
          width: 1920,
          height: 1080
        }
      ],
      ignore: {
        atrule: ['@font-face'],
        rule: [
          '.c-newsletter__frame',
          /\.pswp.*/
        ]
      }
    })

    // run critical css through postcss
    const processed = await postcss([
      autoprefixer({
        overrideBrowserslist: [
          '> 1%, not IE 11'
        ]
      }),
      cssnano({
        preset: 'default'
      })
    ]).process(css)

    // Append critical styles to head using JSDOM
    const dom = new JSDOM(content)
    const { document } = dom.window
    const style = document.querySelector('#critical')
    style.innerHTML = processed

    // Return serialized JSDOM
    return dom.serialize()
  }

  return content
}
