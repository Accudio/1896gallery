const { JSDOM } = require('jsdom')

module.exports = async function(content, outputPath) {
  if (outputPath && outputPath.endsWith('.html')) {
    const dom = new JSDOM(content)
    const { document } = dom.window

    // get all elements to append
    const elements = [...document.querySelectorAll('[data-append]')]

    // append each element to the given selector
    elements.forEach((element) => {
      const selector = element.dataset.append
      document.querySelector(selector).appendChild(element)

      // replace template with its content
      element.replaceWith(...element.content.childNodes)
    })

    // return serialized JSDOM
    return dom.serialize()
  }

  return content
}
