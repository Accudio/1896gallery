const { JSDOM } = require('jsdom')

module.exports = async function(content, outputPath) {
  if (outputPath && outputPath.endsWith('.html')) {
    const dom = new JSDOM(content)
    const { document } = dom.window

    const noscript = document.createElement('link')
    noscript.rel = 'stylesheet'
    noscript.href = '/_assets/noscript.css'

    document.getElementById('noscript').appendChild(noscript)

    // return serialized JSDOM
    return dom.serialize()
  }

  return content
}
