// markdown-it
// Options: https://github.com/markdown-it/markdown-it#init-with-presets-and-options
module.exports = require('markdown-it')({
  html: true,
  breaks: true,
  typographer: true
})
