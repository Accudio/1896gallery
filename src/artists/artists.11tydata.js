module.exports = {
  eleventyComputed: {
    works: data => {
      let nums = {
        available: 1,
        sold: -1
      }
      return data.artist.acf.works.works.sort(function(a, b) {
        return -(nums[a.availability] - nums[b.availability]) / 2
      })
    },
    currentItem: data => {
      return data.artist
    },
    socialImage: data => {
      return data.artist.acf.acf_thumbnail
    }
  }
}
