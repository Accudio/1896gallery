// You can now require Twelvety options using @12ty
const twelvety = require('@12ty')

module.exports = {
  layout: false,
  eleventyExcludeFromCollections: true,
  eleventyComputed: {
    permalink: data => {
      return twelvety.env !== 'production' ? data.permalink : false
    }
  }
}
