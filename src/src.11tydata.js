const pages = require('./_data/pages')

module.exports = {
  eleventyComputed: {
    newsletter: data => typeof data.newsletter !== 'undefined' ? data.newsletter : true,
    contact: data => typeof data.contact !== 'undefined' ? data.contact : true,
    currentItem: async data => {
      const slug = data.wpSlug || (data.wpPage && data.wpPage.slug) || false
      if (slug) {
        const allPages = await pages()
        return allPages.filter(item => {
          return item['slug'] === slug
        })[0]
      }
    }
  }
}
