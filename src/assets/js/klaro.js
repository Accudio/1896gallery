window.klaroConfig = {
  privacyPolicy: '/privacy-policy/',
  cookieName: '1896consent',
  default: true,
  hideDeclineAll: true,
  translations: {
    en: {
      acceptAll: 'Accept all',
      acceptSelected: 'Accept selected',
      app: {
        disableAll: {
          description: 'Use this switch to enable or disable all services.',
          title: 'Enable or disable all services'
        },
        optOut: {
          description: 'This services is loaded by default (but you can opt out)',
          title: '(opt-out)'
        },
        purpose: 'purpose',
        purposes: 'purposes',
        required: {
          description: 'This service(s) is always required',
          title: '(always required)'
        }
      },
      close: 'Close',
      consentModal: {
        description: 'Here you can assess and customize the services that we\'d like to use on this website. You\'re in charge! Enable or disable services as you see fit.',
        privacyPolicy: {
          name: 'privacy policy',
          text: 'To learn more, please read our {privacyPolicy}.'
        },
        title: 'Services we would like to use'
      },
      consentNotice: {
        changeDescription: 'There were changes since your last visit, please renew your consent.',
        configure: 'configure',
        description: 'Hi! Could we please enable some additional services for {purposes}?',
        imprint: {
          name: 'imprint'
        },
        learnMore: 'Let me choose',
        privacyPolicy: {
          name: 'privacy policy'
        },
        testing: 'Testing mode!'
      },
      decline: 'I decline',
      ok: 'That\'s ok',
      poweredBy: '1896 Gallery',
      purposeItem: {
        app: 'service',
        apps: 'services'
      },
      purposes: {
        analytics: {
          title: 'Analytics',
          description: 'These services track your visit to and usage of our website. We use this data to improve the experience of future visitors.'
        }
      },
      save: 'Save',
      analytics: {
        description: 'Custom analytics solution for 1896 Gallery. This information is only accessible to those working for 1896 Gallery and is not accessible to any other companies or inidividuals.'
      }
    }
  },
  apps: [
    {
      name: 'analytics',
      default: true,
      title: '1896 Gallery Analytics',
      purposes: ['analytics'],
      cookies: [
        [/^cly_.*$/, '/']
      ],
      required: false,
      optOut: true,
      onlyOnce: false,
      callback: function(consent, app) {
        if (window.Countly.check_consent) {
          const consentStatus = window.Countly.check_consent('sessions')
          if (consent && !consentStatus) {
            window.Countly.q.push(['add_consent', window.Countly.consentItems])
          }
          if (!consent && consentStatus) {
            window.Countly.q.push(['remove_consent', window.Countly.consentItems])
            if (localStorage) {
              localStorage.removeItem('cly_queue')
              localStorage.removeItem('cly_event')
              localStorage.removeItem('cly_id')
              localStorage.removeItem('cly_session')
            }
          }
        }
      }
    }
  ]
}

// import klaro
window.klaro = require('klaro/dist/klaro-no-translations-no-css')

const links = document.querySelectorAll('a[href="#consent"]')
if (links) {
  links.forEach(el => {
    el.addEventListener('click', () => {
      window.klaro.show()
    })
  })
}

window.depsLoad.klaro = true
