window.PhotoSwipe = require('photoswipe/dist/photoswipe')
window.PhotoSwipeUI = require('photoswipe/dist/photoswipe-ui-default')

const PhotoSwipe = window.PhotoSwipe
const PhotoSwipeUI = window.PhotoSwipeUI

let galleries = {}

parseGalleries()
const lightboxes = document.querySelectorAll('[data-lightbox]')
lightboxes.forEach(function(lightbox) {
  lightbox.addEventListener('click', onClick)
})

const hashData = parseURL()
if (hashData.pid && hashData.gid) {
  open(hashData.gid, hashData.pid - 1, true)
}

function parseGalleries() {
  document.querySelectorAll('[data-lightbox]').forEach(function(el) {
    const gallery = el.getAttribute('data-lightbox')
    if (!galleries[gallery]) {
      galleries[gallery] = []
    }

    const size = el.getAttribute('data-size').split('x')
    const item = {
      el: el,
      src: el.getAttribute('href'),
      w: parseInt(size[0], 10),
      h: parseInt(size[1], 10)
    }

    if (el.getAttribute('data-caption')) {
      item.title = el.getAttribute('data-caption')
    }

    const imgTag = el.querySelector('img')
    if (imgTag) {
      item.msrc = imgTag.getAttribute('src')
    }

    galleries[gallery].push(item)
  })
}

function onClick(e) {
  e = e || window.event
  e.preventDefault ? e.preventDefault() : e.returnValue = false

  const eTarget = e.target || e.srcElement
  const clickedItem = eTarget.closest('a')

  if (!clickedItem) return

  // find gallery in galleries object
  const gallery = clickedItem.getAttribute('data-lightbox')
  let index
  galleries[gallery].some(function(item, i) {
    if (item.el === clickedItem) {
      index = i
      return true
    }
    return false
  })

  if (gallery && index >= 0) {
    return open(gallery, index)
  }
  return false
}

function parseURL() {
  const hash = window.location.hash.substring(1)
  const params = {}

  if (hash.length < 5) {
    return params
  }

  const vars = hash.split('&')
  vars.forEach(function(v) {
    if (v) {
      const pair = v.split('=')
      if (pair.length === 2) {
        params[pair[0]] = pair[1]
      }
    }
  })

  return params
}

function open(galleryName, index, disableAnimation) {
  const pswp = document.querySelector('.pswp')

  const options = {
    index: index,
    galleryUID: galleryName,
    showHideOpacity: true,
    getThumbBoundsFn: false
  }

  const prefersReducedMotion = window.matchMedia('(prefers-reduced-motion: reduce)').matches
  if (disableAnimation || prefersReducedMotion) {
    options.showAnimationDuration = 0
  }

  const gallery = new PhotoSwipe(pswp, PhotoSwipeUI, galleries[galleryName], options)
  gallery.init()
}

window.depsLoad.photoswipe = true
