// gulp
const { parallel, watch } = require('gulp')

// tasks
const fonts = require('./tasks/fonts')
const sass = require('./tasks/sass')
const js = require('./tasks/javascript')

const watcher = () => {
  watch('./src/assets/scss/**/*.scss', { ignoreInitial: true }, sass)
  watch('./src/assets/js/**/*.js', { ignoreInitial: true }, js)
}

exports.default = parallel(fonts, sass, js)

exports.watch = watcher
exports.fonts = fonts
exports.sass = sass
exports.js = js
