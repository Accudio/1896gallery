const path = require('path')

// modules
const { dest, src } = require('gulp')
const browserify = require('browserify')
const log = require('gulplog')
const tap = require('gulp-tap')
const buffer = require('gulp-buffer')
const babel = require('gulp-babel')
const sourcemaps = require('gulp-sourcemaps')
const terser = require('gulp-terser')

// twelvety options from .twelvety.js
const twelvety = require('../.twelvety.js')

// whether we should compile for production
const isProduction = process.env.NODE_ENV === 'production'

const config = {
  entry: './src/assets/js/',
  babelIgnore: [
    'instantpage'
  ]
}

const js = () => {
  let stream = src(config.entry + '*.js', { read: false }) // no need to read file because browserify does

  // transform file objects using gulp-tap plugin and pass to browserify
  stream = stream
    .pipe(tap(file => {
      log.info('bundling ' + file.path)
      // replace file contents with browserify's bundle stream
      let bundle = browserify(file.path, {
        debug: true,
        paths: [
          path.join(process.cwd(), twelvety.dir.input, twelvety.dir.js),
          path.join(process.cwd(), 'node_modules')
        ]
      })
      const fileName = path.basename(file.path, '.js')
      if (isProduction && fileName !== 'klaro') {
        bundle = bundle.plugin('tinyify')
      }

      file.contents = bundle.bundle()
    }))

  // transform streaming contents into buffer contents (because gulp-sourcemaps does not support streaming contents)
  stream = stream
    .pipe(buffer())

  // initialise sourcemaps
  if (!isProduction) {
    stream = stream
      .pipe(sourcemaps.init())
  }

  // if production, run through babel
  if (isProduction) {
    const ignore = config.babelIgnore.map(name => {
      return config.entry + name + '.js'
    })
    stream = stream
      .pipe(babel({
        ignore
      }))
  }

  // if production, run through terser to minimise
  if (isProduction) {
    stream = stream
      .pipe(terser())
  }

  // write sourcemaps
  if (!isProduction) {
    stream = stream
      .pipe(sourcemaps.write())
  }

  return stream.pipe(dest('./dist/_assets', { sourceMaps: !isProduction }))
}

module.exports = js
