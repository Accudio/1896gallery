const path = require('path')

// modules
const { dest, src } = require('gulp')
const sassProcessor = require('gulp-sass')
const sassGlob = require('gulp-sass-glob')
const postcss = require('gulp-postcss')
const cleanCSS = require('gulp-clean-css')
const sourcemaps = require('gulp-sourcemaps')

// twelvety options from .twelvety.js
const twelvety = require('../.twelvety.js')

// use node-sass for performance and globbing
sassProcessor.compiler = require('node-sass')

// whether we should compile for production
const isProduction = process.env.NODE_ENV === 'production'

const sass = () => {
  let stream = src('./src/assets/scss/*.scss')

  // initialise sourcemaps
  if (!isProduction) {
    stream = stream
      .pipe(sourcemaps.init())
  }

  // sass
  stream = stream
    .pipe(sassGlob())
    .pipe(sassProcessor({
      includePaths: [
        path.join(process.cwd(), twelvety.dir.input, twelvety.dir.styles),
        path.join(process.cwd(), 'node_modules')
      ],
      indentedSyntax: false
    })
      .on('error', sassProcessor.logError))

  // if production, run through postcss
  if (isProduction) {
    stream = stream
      .pipe(postcss())
  }

  // write sourcemaps
  if (!isProduction) {
    stream = stream
      .pipe(sourcemaps.write())
  }

  // if production, clean css
  if (isProduction) {
    stream = stream
      .pipe(
        cleanCSS(
          isProduction ? {
            level: 2
          } : {}
        )
      )
  }

  return stream.pipe(dest('./dist/_assets'))
}

module.exports = sass
